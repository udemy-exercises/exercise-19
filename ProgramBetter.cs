using System.IO;

class Program
{
   static void Main()
   {
      string path = "/home/$USER/Documents/Programming/C#/udemy-exercises/exercise-19";
      string[] fileData = File.ReadAllLines(path + "/file.csv");

      string[][] products = new string[fileData.Length][];
      
      for (int i = 0; i < fileData.Length; i++)
      {
         products[i] = fileData[i].Split(',');
      }

      double[] productsPrice = new double[products.Length];
      int[] productsAmount = new int[products.Length];

      for (int i = 0; i < products.Length; i++)
      {
         productsPrice[i] = double.Parse(products[i][1]);
         productsAmount[i] = int.Parse(products[i][2]);
      }

      Directory.CreateDirectory(path + "/out");

      // I make use of the "using" block here to avoid an expcetion "The process cannot access the file because it is being used by another process" when trying to write to it later
      using (File.Create(path + "/out" + "/summary.csv"));

      File.WriteAllText((path + "/out" + "/summary.csv"), ""); // I use "WriteAllText" instead of "AppendAllText" here to make sure the file is empty
      for (int i = 0; i < products.Length; i++)
      {
         File.AppendAllText((path + "/out" + "/summary.csv"), $"{products[i][0]}, {productsPrice[i] * productsAmount[i]}\n");
      }
   }
}